import random
import networkx as nx
import matplotlib.pyplot as plt
import uuid


class RandomGraphGenerator:
	"""
	Class for generation of random graph and its visualization
	"""
	def __init__(self):
		self.ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

	def generate_random_graph(self, num_of_nodes: int, graph_degree: int) -> (nx.Graph, str):
		"""
		Creates random nx.Graph object of given parameters and saves it as png file.
		:param num_of_nodes: number of graph nodes
		:param graph_degree: max degree of nodes (number of node neighbours)
		:return: nx.Graph object and a path to its graphic representation
		"""
		G = nx.Graph()

		G.add_nodes_from(self.ALPHABET[:num_of_nodes])

		nodes_to_draw_from = []
		for node in G.nodes:
			if node != 'A':
				node_to_connect = random.choice(nodes_to_draw_from)
				G.add_edge(node, node_to_connect, weight=round(random.uniform(1, 10), 1))
				if len(G.adj[node_to_connect]) == graph_degree:
					nodes_to_draw_from.remove(node_to_connect)
			nodes_to_draw_from.append(node)

		for _ in range(num_of_nodes//2):
			node = random.choice(nodes_to_draw_from)
			not_neighbors = nodes_to_draw_from.copy()
			not_neighbors.remove(node)
			for n in G.neighbors(node):
				if n in not_neighbors:
					not_neighbors.remove(n)
			if not_neighbors is []:
				continue
			node_to_connect = random.choice(not_neighbors)
			G.add_edge(node, node_to_connect, weight=round(random.uniform(1, 10), 1))
			if len(G.adj[node]) == graph_degree:
				nodes_to_draw_from.remove(node)
			if len(G.adj[node_to_connect]) == graph_degree:
				nodes_to_draw_from.remove(node_to_connect)

		pos = nx.spring_layout(G)
		nx.draw(G, pos, with_labels=True)
		labels = nx.get_edge_attributes(G, 'weight')
		nx.draw_networkx_edge_labels(G, pos, edge_labels=labels)
		name = uuid.uuid1()
		path = "../developer_data/xml_modules/images/%s.png" % name
		plt.savefig(path)
		plt.clf()
		# plt.show()

		return G, path
