import networkx as nx

from Modules.graph_generator import RandomGraphGenerator
from Modules.general_module_functions import ModuleHelper


class DijkstraTaskGenerator:
    """
    Class for creating dijkstra task and saving it as xml template.
    """

    @staticmethod
    def generate(num_of_nodes: int = 8, graph_degree: int = 3) -> (str, str):
        """
        Generates random graph and counts dijkstra distances for each node (starting from 'A').
        :param num_of_nodes: number of graph nodes
        :param graph_degree: max degree of nodes (number of node neighbours)
        :return: Dijkstra distances of each node from 'A' as result string (e. g. 'B:1.4,D:4.5,C:5.9') and path to graph
        visual representation.
        """

        RGG = RandomGraphGenerator()
        graph, path = RGG.generate_random_graph(num_of_nodes, graph_degree)

        dijkstra_distances = nx.single_source_dijkstra_path_length(graph, 'A')
        print(dijkstra_distances)
        result = ""
        for node in dijkstra_distances.keys():
            if node == 'A':
                continue
            result += '%s:%.1f,' % (node, round(dijkstra_distances[node], 1))
        result = result[:-1]
        print(result)
        return result, path

    @staticmethod
    def create_template_xml(num_of_versions: int, num_of_nodes: int = 8, graph_degree: int = 3):
        """
        Creates number of dijkstra tasks in xml file.
        :param num_of_versions: number of task version to generate
        :param num_of_nodes: number of graph nodes
        :param graph_degree: max degree of nodes (number of node neighbours)
        """
        description = "Dany jest poniższy graf"
        question_text = "Podaj w kolejności wierzchołki wybierane przy poszukiwaniu min. drogi przez alg. Dijkstry i " \
                        "przyznawane im odległości od wierzchołka A."
        ModuleHelper.create_xml_template_drawn_tasks(num_of_versions, description, question_text,
                                                     DijkstraTaskGenerator.generate, num_of_nodes, graph_degree,
                                                     output_path="../developer_data/xml_modules/dijkstra_task.xml")
