from Modules.dijkstra_module import DijkstraTaskGenerator
from Modules.general_module_functions import ModuleHelper


class ExtendedDijkstraTaskGenerator(DijkstraTaskGenerator):
    """
    Class for creating dijkstra task and saving it as xml template.
    """

    @staticmethod
    def generate(num_of_nodes: int = 8, graph_degree: int = 3) -> (list, str):
        """
        Generates random graph and counts dijkstra distances for each node (starting from 'A').
        :param num_of_nodes: number of graph nodes
        :param graph_degree: max degree of nodes (number of node neighbours)
        :return: list of answers for each question in task and path to graph
        visual representation.
        """
        answer1, path = DijkstraTaskGenerator.generate(num_of_nodes, graph_degree)
        result = [num_of_nodes, graph_degree, answer1]
        return result, path

    @staticmethod
    def create_template_xml(num_of_versions: int, num_of_nodes: int = 8, graph_degree: int = 3):
        """
        Creates number of dijkstra tasks in xml file.
        :param num_of_versions: number of task version to generate
        :param num_of_nodes: number of graph nodes
        :param graph_degree: max degree of nodes (number of node neighbours)
        """
        description = "Dany jest poniższy graf"
        questions_text = ["Podaj liczbę wierzchołków grafu", "Podaj stopień grafu",
                          "Podaj w kolejności wierzchołki wybierane przy poszukiwaniu min. drogi przez alg. Dijkstry i "
                          "przyznawane im odległości od wierzchołka A."]
        ModuleHelper.create_xml_template_drawn_tasks(num_of_versions, description, questions_text,
                                                     ExtendedDijkstraTaskGenerator.generate, num_of_nodes, graph_degree,
                                                     output_path="../developer_data/xml_modules/extended_dijkstra_task.xml",
                                                     num_of_questions=3)
