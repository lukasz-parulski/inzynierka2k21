import xml.etree.cElementTree as ET
from typing import List, Union


class ModuleHelper:
    """
    Class containing universal functions used in modules.
    """
    @staticmethod
    def create_xml_template_drawn_tasks(num_of_versions: int, description_text: str, question_text: Union[str, List[str]],
                                        generate_function, *args, output_path: str = "task.xml",
                                        number_of_exercises_to_choose: int = 1, num_of_questions: int = 1):
        """
        Creates number of dijkstra tasks in xml file.
        :param num_of_versions: number of task version to generate
        :param description_text: description of the task
        :param question_text: question of generated task
        :param generate_function: function that generates image and answer to task
        :param output_path: path to xml to save created task template
        :param number_of_exercises_to_choose: number of exercises that will be chosen for final exam sheet from those in
         template
        :param num_of_questions: number of questions in single task
        """
        root = ET.Element("template")
        drawn_list = ET.SubElement(root, "drawListExercise")
        drawn_list.set("numberOfExercisesToChoose", str(number_of_exercises_to_choose))
        for i in range(num_of_versions):
            answer, filename = generate_function(*args)
            ModuleHelper.create_multiple_question_text_exercise(drawn_list, answer, question_text, filename,
                                                                description_text, num_of_questions)
        tree = ET.ElementTree(root)
        with open(output_path, 'wb') as f:
            tree.write(f, encoding='utf-8', xml_declaration=True)

    @staticmethod
    def create_multiple_question_text_exercise(root: ET.Element, answers: Union[str, list],
                                               questions_text: Union[str, List[str]], filename: str,
                                               description_text: str, num_of_questions: int = 1):
        """
        Creates dijkstra task in xml template format and saves it to file.
        :param root: root xml element to add to
        :param answers: either single answer or list of answers for generated task
        :param questions_text: either single question or list of questions for generated task
        :param filename: path for task image
        :param description_text: description of the task
        :param num_of_questions: number of questions in task
        """
        answers = [answers] if not isinstance(answers, list) else answers
        questions_text = [questions_text] if isinstance(questions_text, str) else questions_text

        assert len(answers) == num_of_questions, "Wrong number of answers"
        assert len(questions_text) == num_of_questions, "Wrong number of questions"

        exercise = ET.SubElement(root, "textExercise")
        exercise.set("numberOfAnswers", str(num_of_questions))
        image = ET.SubElement(exercise, "image")
        filename = filename[filename.rindex('/') + 1:]
        image.text = filename
        description = ET.SubElement(exercise, "description")
        description.text = description_text
        for i in range(num_of_questions):
            answer = answers[i] if isinstance(answers[i], str) else str(answers[i])
            text_question = ET.SubElement(exercise, "textQuestion")
            text_question.set("answer", answer)
            text_question.text = questions_text[i]
