from Modules.dijkstra_module import DijkstraTaskGenerator
from Modules.dijkstra_module import ExtendedDijkstraTaskGenerator


if __name__ == "__main__":
    DijkstraTaskGenerator.create_template_xml(5)
    ExtendedDijkstraTaskGenerator.create_template_xml(5)

