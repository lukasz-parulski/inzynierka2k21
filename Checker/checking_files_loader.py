import csv
from xml.etree.ElementTree import parse


class CheckingFileLoader:

    XML_EXAM_TAG = "exam"
    XML_EXAM_ID_TAG = "id"
    XML_USER_ID_TAG = "userId"
    XML_TASK_TAG = "task"
    XML_ANSWERS_TAG = "answers"
    XML_VALUE_TAG = "value"

    def __init__(self, student_ids_path):
        self._students_ids_path = student_ids_path
        self._students_answer_list = []

    @staticmethod
    def load_answer_key_file(index):
        with open(str("../data/answer_keys/"+index+"_answer_key.csv"), 'r', encoding='UTF8') as f:
            reader = csv.reader(f, delimiter=";")
            key = []
            for ans in reader:
                key.append(ans)
            return key

    def load_answer_list_file_google(self):
        with open(str(self._students_ids_path + "/reponse.csv"), 'r', encoding='UTF8') as f:
            reader = csv.reader(f, delimiter=',')
            next(reader)
            for ans in reader:
                student = []
                for word in ans:
                    student.append(word)
                self._students_answer_list.append(student)

    def load_answer_list_file_service(self):
        root = parse(self._students_ids_path + "/answer_list.xml").getroot()
        exams = root.findall(self.XML_EXAM_TAG)
        for exam in exams:
            student = []
            exam_id = exam.get(self.XML_EXAM_ID_TAG)
            student.append(exam_id)
            for elem in exam:
                if elem.tag == self.XML_TASK_TAG:
                    student.extend(self._load_xml_task(elem))
            self._students_answer_list.append(student)

    def _load_xml_task(self, elem):
        answers = elem.find(self.XML_ANSWERS_TAG)
        values = answers.findall(self.XML_VALUE_TAG)
        student = []
        for value in values:
            student.append(value.text)
        return student

    def get_students_answer_list(self):
        return self._students_answer_list



