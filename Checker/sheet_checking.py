import csv
from xml.etree.ElementTree import parse
from checking_files_loader import CheckingFileLoader


class SheetChecking:

    def __init__(self, student_ids_path):
        self._loader = CheckingFileLoader(student_ids_path)

    @staticmethod
    def _polish_to_english_conversion(student_list):
        for i in range(0, len(student_list)-1):
            if student_list[i] == "Prawda":
                student_list[i] = "True"
            if student_list[i] == "Fałsz":
                student_list[i] = "False"

    @staticmethod
    def _compare_answers( key, student_list):
        score_sheet = []
        student_score = 0
        for question in key:
            exercise_is_correct = True
            for answer in question:
                if answer != "":
                    if answer != student_list[0] or student_list[0] == "":
                        score_sheet.append(answer + " | " + student_list[0] + " | 0")
                        exercise_is_correct = False
                    else:
                        score_sheet.append(answer + " | " + student_list[0] + " | 1")
                    student_list.pop(0)

            if exercise_is_correct is True:
                student_score += 1
                score_sheet.append("Points for this task: 1")
            else:
                score_sheet.append("Points for this task: 0")

        return student_score, score_sheet

    @staticmethod
    def _print_score_sheet_txt(index, score_sheet, score, end_time=0):
        with open(str("../data/final_score.txt"), 'a', encoding='UTF8') as f:

            f.write("Student " + index + "\n")
            f.write("Exam end time: " + str(end_time) + "\n")
            for exercise in score_sheet:
                f.write(exercise + "\n")
            f.write("Final points: " + str(score) + "\n \n \n")

    @staticmethod
    def _print_score_sheet_csv(index, score_sheet, score, end_time = 0):
        with open(str("../data/final_score.csv"), 'a', encoding='UTF8', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(["Student " + index])
            writer.writerow(["Exam end time: " + str(end_time)])
            for exercise in score_sheet:
                writer.writerow([exercise])
            writer.writerow(["Final points: " + str(score) + "\n \n \n"])

    def check_student_google_sheets(self):
        self._loader.load_answer_list_file_google()
        student_list = self._loader.get_students_answer_list()

        for student in student_list:
            end_time = student.pop(0)
            index = student.pop(0)
            key = self._loader.load_answer_key_file(index)
            self._polish_to_english_conversion(student)
            score, score_sheet = self._compare_answers(key, student)
            self._print_score_sheet_txt(index, score_sheet, score, end_time)

    def check_student_service_sheets(self):
        self._loader.load_answer_list_file_service()
        student_list = self._loader.get_students_answer_list()

        for student in student_list:
            index = student.pop(0)
            key = self._loader.load_answer_key_file(index)
            self._polish_to_english_conversion(student)
            score, score_sheet = self._compare_answers(key, student)
            self._print_score_sheet_txt(index, score_sheet, score)
