import {FolderDataId} from "../model/folderDataId";

export function clear() {
    deleteAllTriggers();
    deleteSheets();
}

export function deleteAllTriggers() {
    const triggers = ScriptApp.getProjectTriggers();
    for (const trigger of triggers) {
        ScriptApp.deleteTrigger(trigger);
    }
}

export function deleteSheets() {
    const spreadSheet = SpreadsheetApp.openById(FolderDataId.AnswersSpreadSheetFileId);
    const sheets = spreadSheet.getSheets();

    for(const sheet of sheets) {
        if(sheet.getName() !== 'AISD_2021') {
            const formUrl = sheet.getFormUrl();
            FormApp.openByUrl(formUrl).removeDestination();
            spreadSheet.deleteSheet(sheet);
        }
    }
}
