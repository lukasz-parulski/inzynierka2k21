/**
 * This function lists all filed ID's from given folder and return them as array of strings
 * @param directoryID - ID of the directory what files should be listed
 * @return returns the array of string containing each files ID from given folder
 */
import {FolderDataId} from "../model/folderDataId";

export function listAllFilesIDFromGdrive(directoryID: FolderDataId): string[] {
    const gFilesIterator = DriveApp.getFolderById(directoryID).getFiles();
    const gFilesId: string[] = [];

    while(gFilesIterator.hasNext()){
        const gFile = gFilesIterator.next();
        gFilesId.push(gFile.getId());
    }

    return gFilesId;
}

/**
 * Creates folder in Google Drive
 * @param directoryID - parent directory ID
 * @param folderName - name of the new folder
 * @return returns the Folder object from Google Drive
 */
export function createFolder(directoryID: FolderDataId, folderName: string): GoogleAppsScript.Drive.Folder {
    const parentFolder = DriveApp.getFolderById(directoryID);
    return parentFolder.createFolder(folderName);
}

/**
 * Function that find file from google drive by filename and the specified folder
 * @param folder - folder where file should be
 * @param fileName - the searching filename
 * @return return Google File object
 */
export function findGfileByName(folder, fileName): GoogleAppsScript.Drive.File {
    const filesFromFolder = DriveApp.getFolderById(folder).getFiles();
    let file: GoogleAppsScript.Drive.File;

    while(filesFromFolder.hasNext()){
        const tmp = filesFromFolder.next();

        if (tmp.getName() === fileName) {
            file = tmp;
        }
    }

    return file;
}
