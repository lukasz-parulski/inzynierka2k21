import {FolderDataId} from "../model/folderDataId";
import {listAllFilesIDFromGdrive} from "./gdrive.util";

/**
 * This function extracts all ID's of all XLS files located in give folder
 * Then takes the first XLS file (the newest one) and extracts all data as a string
 * Then split the string to array of separate indexes and remove header ('index')
 *
 * @param indexesFolder - Folder with all indexes files;
 * @return - return the String array of each index from csv file
 */
export function getIndexesFromXlsFile(indexesFolder: FolderDataId): string[] {
    const indexesFilesID: string[] = listAllFilesIDFromGdrive(indexesFolder);
    const firstXlsFile = DriveApp.getFileById(indexesFilesID[0]);
    const xlsData = firstXlsFile.getBlob().getDataAsString();
    const indexes: string[] = xlsData.split("\r\n");
    indexes.shift(); // for removing the header ('index')
    return indexes;
 }
