/**
 * @enum TaskType - enum that contains all possible task types
 * @property TrueFalseType - task with one question or multiple questions that has only true or false answer
 * @property ClosedType - task with one question and multiple answers where only one can be chosen
 * @property GraphType - task with one or multiple questions that also contains image with graph
 */
export enum TaskType {
    TrueFalseType = 'Tak-Nie',
    MultiplyChoiceType = 'Wielokrotnego',
    ClosedType = 'Zamkniete',
    OpenedType = 'Otwarte'
}
