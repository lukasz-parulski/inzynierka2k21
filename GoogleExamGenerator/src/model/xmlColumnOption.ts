/**
 * @enum XmlColumnOption - enumeration object for helping parsing XML file
 * @property Title - the Title of task (TODO maybe use only @Question instead?)
 * @property Question - if XML task contains only one question then the name of attribute will be "Question"
 *
 * @propert Questions - if XML task contains multiple questions then each question
 *                      will have attribute name of "Questions" (ex.
 *                      <column name="Questions"><value>"Some Question 1"</value></column>
 *                      <column name="Questions"><value>"Some Question 2"</value></column>)
 *
 * @property Answers - possible answers of the task, each attribute contains name of "Answer" (ex.
 *                      <column name="Answers"><value>"Some Answer 1"</value></column>
 *                      <column name="Answers"><value>"Some Answer 2"</value></column>)
 * @property ImgName - attribute that name is "ImageName" that contains the name of the image from g-drive
 */
export enum XmlColumnOption {
    Title = 'Title',
    Question = 'Question',
    Questions = 'Questions',
    Answer = 'Answer',
    Answers = 'Answers',
    ImgName = 'ImageName'
}
