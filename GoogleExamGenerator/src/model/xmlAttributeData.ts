/**
 * @interface XmlAttributeData - interface that helps parsing data from XML file
 * @property attrName - name of XML the attribute
 * @property attrValue - vaue of XML the attribute
 *
 * @example
 * <type name="Type"><value><![CDATA[Grafowe]]></value>
 * where attrName is "Type" (name="Type") and attrValue is "Grafowe" ([!CDATA[Grafowe]])
 */
export interface XmlAttributeData {
    attrName: string,
    attrValue: string
}
