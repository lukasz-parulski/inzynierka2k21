/**
 * @enum FolderDataId enum object that contains direct folders and files ID from google drive
 * to easy get access to data from g-drive
 *
 * @property XmlFilesFolderId - ID to folder from g-drive that contains all generated XML files with tasks
 * @property ImagesFilesFolderId - ID to folder from g-drive that contains all images needed for tasks with image
 * @property GoogleFormsFolderId - ID to folder from g-drive where all generated Google Forms exams will drop
 * @property AnswersSpreadSheetFileId - ID to file that contains spreadsheet with answers from students
 * @property IndexesSpreadSheetFileId - ID to file that contains all student indexes
 */
export enum FolderDataId {
    Testing = '1GBmrc5Csb2hrkm63AdYX55eUHf5-Pxe8',
    XmlFilesFolderId = '1hISwkHtJATDWUda8gexqo2UpD8c0YUPr',
    ImagesFilesFolderId = '1WAHX7Pu98a-snz8BX_dd-joEzG8uoZrR',
    GoogleFormsFilesFolderId = '1Tl1Gin_25XPyU6NUorxOeyHApWCUCqY6',
    AnswersSpreadSheetFileId = '1Psp2va2e4OuzHSju7g2TXMztkx4iz78z6eupfrWwVfI',
    IndexesFileFolderId = '1RbBpUHrrQ9Jw6EHA3bCOMkQaKg4pOdUc'
}
