export class PropertiesCreator {

    private static MAIN_RESPONSE_SHEET_NAME = 'AISD_2021';

    static createMainResponseSheetNameProperty() {
        PropertiesService.getUserProperties().setProperty('MAIN_RESPONSE_SHEET_NAME', this.MAIN_RESPONSE_SHEET_NAME);
    }
}
