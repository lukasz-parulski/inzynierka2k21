import {FolderDataId} from '../model/folderDataId';
import {XmlAttributeData} from '../model/xmlAttributeData';
import {TaskType} from '../model/taskType';
import {findGfileByName} from '../utils/gdrive.util';
import {XmlColumnOption} from '../model/xmlColumnOption';
/**
 * TaskService is the main service of creating tasks on the Google Form from XML Schema.
 * Firstly XML file is parsed by iterating through each XML column (function iterateThroughColumns is called)
 * Then one by one attribute name with the value are extracted to the XmlAttributeData interface.
 * After, all variables (imageName, question, questions, answers) depending on extracted attribute values are set.
 * Then, depending on the extracted task type appropriate task function are called what creates task on google form
 * and sets task parameters with earlier defined variables.
 */
export class TaskService {

    private gForm: GoogleAppsScript.Forms.Form;

    private title: string = '';
    private question: string = '';
    private imagesNames: string[] = [];

    private answers: string[] = [];
    private questions: string[] = [];

    addTasks(xmlFile: string, gForm: GoogleAppsScript.Forms.Form) {
        this.gForm = gForm;

        const xmlDoc = XmlService.parse(xmlFile);
        const response = xmlDoc.getRootElement();
        const tasks = response.getChildren('task');

        this.gForm.addTextItem()
            .setTitle('Wpisz swój numer albumu (e.x 123456)')
            .setRequired(true);

        for (const task of tasks) {
            const typeChild = task.getChild('type');
            const typeOfQuestion = typeChild.getChild('value').getText();
            const columns = typeChild.getChildren('column');

            this.iterateThroughColumns(columns);

            switch (typeOfQuestion) {
                case TaskType.TrueFalseType:
                    this.createTrueFalseTask();
                    break;
                case TaskType.MultiplyChoiceType:
                    this.createMultiplyChoiceTask();
                    break;
                case TaskType.ClosedType:
                    this.createClosedTask();
                    break;
                case TaskType.OpenedType:
                    this.createOpenedTask();
                    break;
                default:
                    break;
            }

            this.title = '';
            this.question = '';
            this.imagesNames = [];
            this.answers = [];
            this.questions = [];
        }
    }

    iterateThroughColumns(columns) {
        for(const column of columns) {
            const xmlAttrData: XmlAttributeData = {
                attrName: column.getAttribute('name').getValue(),
                attrValue: column.getChild('value').getText()
            }

            switch(xmlAttrData.attrName) {
                case XmlColumnOption.Title:
                    this.title = xmlAttrData.attrValue
                    break;
                case XmlColumnOption.ImgName:
                    this.imagesNames.push(xmlAttrData.attrValue);
                    break;
                case XmlColumnOption.Question:
                    this.question = xmlAttrData.attrValue
                    break;
                case XmlColumnOption.Questions:
                    this.questions.push(xmlAttrData.attrValue);
                    break;
                case XmlColumnOption.Answers:
                    this.answers.push(xmlAttrData.attrValue);
            }
        }
    }

    createMultiplyChoiceTask() {
        this.gForm.addCheckboxItem()
            .setTitle(this.title)
            .setChoiceValues(this.answers);
    }

    createTrueFalseTask() {
        this.gForm.addGridItem()
            .setTitle(this.title)
            .setRows(this.questions)
            .setColumns(['Prawda', 'Fałsz']);
    }

    createClosedTask() {
        this.gForm.addMultipleChoiceItem()
            .setTitle(this.question)
            .setChoiceValues(this.answers);
    }

    createOpenedTask() {
        this.imagesNames.forEach((imageName, index) => {
            const imageFile = findGfileByName(FolderDataId.ImagesFilesFolderId, imageName);
            const task = this.gForm.addImageItem().setImage(imageFile).setTitle(" ");

            if(index === 0) {
                task.setTitle(this.title)
                    .setHelpText('miejsce dla odpowiedzi znajdziesz poniżej')
            }
        })

        for (const question of this.questions) {
            this.gForm.addTextItem()
                .setTitle(question)
        }
    }
}
