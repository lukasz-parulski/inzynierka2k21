import {FolderDataId} from "../model/folderDataId";

export class ResponseService {
    private gForm: GoogleAppsScript.Forms.Form;

    constructor(gForm: GoogleAppsScript.Forms.Form) {
        this.gForm = gForm;
        this.gForm.setDestination(FormApp.DestinationType.SPREADSHEET, FolderDataId.AnswersSpreadSheetFileId);
        this.mapFormResponsesWithSpreadSheet();
    }

    getStudentIndex(): string {
        const formName = DriveApp.getFileById(this.gForm.getId()).getName();
        const studentIndex = formName.match(/\d{6}/g); // wyciągamy ciąg 6 cyfrowy (indeks)
        return studentIndex.toString();
    }

    mapFormResponsesWithSpreadSheet () {
        const spreadSheet = SpreadsheetApp.openById(this.gForm.getDestinationId());
        const activeSheet = spreadSheet.getActiveSheet();
        activeSheet.setName(this.getStudentIndex());
    }

    installTrigger() {
        ScriptApp.newTrigger('onFormSubmit')
                .forSpreadsheet(FolderDataId.AnswersSpreadSheetFileId)
                .onFormSubmit()
                .create();
    }
}

export function onFormSubmit(e) {
    const spreadSheet = e.source;
    const studentIndex = e.values[1];

    const thisSheet = spreadSheet.getSheetByName(studentIndex.toString());
    const mainSheet = spreadSheet.getSheetByName(PropertiesService.getUserProperties().getProperty('MAIN_RESPONSE_SHEET_NAME'));

    const thisSheetValues = thisSheet.getRange(2, 1, 1, thisSheet.getLastColumn()).getValues();

    // bo wpisując index spreadsheet odczytuje to jako
    // liczbe i dodaje .0, przez co spreadsheet mysli ze to data
    thisSheetValues[0][1] = studentIndex.toString();

    let mainSheetLastColumn = mainSheet.getLastColumn();

    // jeżeli spreadsheet jest pusty dodajemy naglowek
    if(mainSheetLastColumn < thisSheet.getLastColumn()) {
        mainSheetLastColumn = thisSheet.getLastColumn();

        const header: String[][] = [["TIMESTAMP", "INDEKS"]]
        for (let i = 1; i <= thisSheet.getLastColumn() - 2; i++) {
            header[0].push("ZAD " + i);
        }
        mainSheet.getRange(mainSheet.getLastRow() + 1, 1, header.length, mainSheetLastColumn).setValues(header);
    }

    mainSheet.getRange(mainSheet.getLastRow() + 1, 1, thisSheetValues.length, mainSheetLastColumn).setValues(thisSheetValues);

    const formUrl = thisSheet.getFormUrl();
    const gForm = FormApp.openByUrl(formUrl);

    // czyszczenie
    gForm.removeDestination();
    spreadSheet.deleteSheet(thisSheet);
}
