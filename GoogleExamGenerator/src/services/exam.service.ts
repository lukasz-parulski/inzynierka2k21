import {TaskService} from './task.service';

/**
 * Class for handling google form exam for the specified student id
 */
export class Exam {

    static index: number = 0;

    EXAM_TITLE: string = 'Egzamin - Algorytmy i Struktury Danych'
    EXAM_FILENAME: string = 'arkusz_egzaminacyjny';
    TEST_EMAIL_ADDRESS: string = 'earkusze.user@gmail.com';
    PG_EMAIL_ADDRESS: string = '@student.pg.edu.pl';

    readonly xmlFile: string;
    private studentId: number;
    private gForm: GoogleAppsScript.Forms.Form;

    /**
     * @param xmlFile - unique xml file given from G-Drive folder as string,
     * containing schema for different task that should be created from
     * @param studentId - unique student id given from csv file
     */
    constructor(xmlFile, studentId) {
        this.xmlFile = xmlFile;
        this.studentId = studentId;
        Exam.index += 1;

        this.createEmptyForm();
    }

    /**
     * gets the google form
     */
    getGoogleForm() {
        return this.gForm;
    }

    /**
     * creates empty g-form file in G-Drive with file name - 'arkusz_egzaminacyjny'
     * plus student ID with the google form title of 'Egzamin - Algorytmy i Struktury Danych'
     */
    private createEmptyForm() {
        this.gForm = FormApp.create(this.EXAM_FILENAME + `_${this.studentId}`)
                            .setTitle(this.EXAM_TITLE);
    }

    /**
     * Move created empty google form to given folder in the G-Drive,
     * @param destinationFolder - destination folder where g-form should be moved
     */
    moveGformToFolder(destinationFolder: GoogleAppsScript.Drive.Folder) {
        const formFile = DriveApp.getFileById(this.gForm.getId());
        destinationFolder.addFile(formFile);
        DriveApp.removeFile(formFile);
    }

    /**
     * Call the TaskService class with the method that add tasks to the current g-form
     * @param taskService - TaskService class handling task adding to the form
     */
    addTasksFromXmlToGform(taskService: TaskService) {
        taskService.addTasks(this.xmlFile, this.gForm);
    }

    /**
     * Send email with the g-form to the specified email address (student ID + @student.pg.edu.pl}
     */
    sendFormByEmail(){
        const url = this.gForm.getPublishedUrl();
        MailApp.sendEmail({
           to: this.TEST_EMAIL_ADDRESS, // this.this.studentId + this.PG_EMAIL_ADDRESS
           subject: 'automatical form sending test',
           htmlBody: url,
         });
    }
}
