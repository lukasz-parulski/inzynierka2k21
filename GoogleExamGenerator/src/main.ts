import {Exam} from './services/exam.service';
import {createFolder, listAllFilesIDFromGdrive} from './utils/gdrive.util';
import {TaskService} from './services/task.service';
import {getIndexesFromXlsFile} from './utils/csv.util';
import {FolderDataId} from "./model/folderDataId";
import {ResponseService} from "./services/response.service";
import {PropertiesCreator} from "./properties/propertiesCreator";

/**
 * Main function where all the processes starts.
 * Firstly the folder with the specified name (function argument) will be created in the google drive,
 * then all the pre-generated XML files ID's will be read to the array of string,
 * then list of student indexes will be read from xls file,
 * then for each XML file the Exam object will be created which will contain Google Form with parsed tasks
 *
 * @param folderName - name of the folder where all google drives should be dropped
 * @return return string of SUCCESS if google forms were created
 */
function main(folderName: string = 'testowy_egzamin'): string {
    const folderForGforms = createFolder(FolderDataId.GoogleFormsFilesFolderId, folderName);
    const xmlFilesId: string[] = listAllFilesIDFromGdrive(FolderDataId.Testing);
    const indexes: string[] = getIndexesFromXlsFile(FolderDataId.IndexesFileFolderId)

    PropertiesCreator.createMainResponseSheetNameProperty();

    const exams: Exam[] = [];
    let counter = 0;

    for(const xmlFileId of xmlFilesId) {
        const xmlFile = DriveApp.getFileById(xmlFileId).getBlob().getDataAsString();
        const exam: Exam = new Exam(xmlFile, indexes[xmlFilesId.indexOf(xmlFileId)]);
        const taskService: TaskService = new TaskService();

        exam.moveGformToFolder(folderForGforms);
        exam.addTasksFromXmlToGform(taskService);
        exam.sendFormByEmail();
        exams.push(exam);

        const responsesService: ResponseService = new ResponseService(exam.getGoogleForm())
        if (counter++ === 0) responsesService.installTrigger();
    }

    return '[SUCCESS] Google Forms were created and sent!';
}
