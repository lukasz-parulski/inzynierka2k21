from .exercise import Exercise


class TextExercise(Exercise):
    def __init__(self, description, texts, answers, images, time):
        self._texts = texts
        self._answers = answers
        super().__init__(description, images, time)

    def get_texts(self):
        return self._texts

    def get_answers(self):
        return self._answers
