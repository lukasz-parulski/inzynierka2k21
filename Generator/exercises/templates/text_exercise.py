import random

from .exercise import ExerciseTemplate
from ..text_exercise import TextExercise


class TextExerciseTemplate(ExerciseTemplate):
    def __init__(self, description, answers_count, texts, answers, images, time):
        self._texts = texts
        self._answers = answers
        super().__init__(description, answers_count, images, time)

    def generate_variation(self):
        variation_texts, variation_answers = zip(*random.sample(list(zip(self._texts, self._answers)),
                                                                self._answers_count))
        return TextExercise(self._description, variation_texts, variation_answers, self._images, self._time)
