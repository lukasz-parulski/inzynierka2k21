import random

from .exercise import ExerciseTemplate
from ..closed_exercise import ClosedExercise


class ClosedExerciseTemplate(ExerciseTemplate):
    def __init__(self, description, answers_count, correct_answers_count, correct_answers, incorrect_answers, images, time):
        self._correct_answers_count = int(correct_answers_count)
        self._correct_answers = correct_answers
        self._incorrect_answers = incorrect_answers
        super().__init__(description, answers_count, images, time)

    def generate_variation(self):
        variation_correct_answers = random.sample(self._correct_answers, self._correct_answers_count)
        variation_incorrect_answers = random.sample(self._incorrect_answers,
                                                    self._answers_count-self._correct_answers_count)
        variation_answers = variation_correct_answers + variation_incorrect_answers
        filler = ["" for zero in variation_incorrect_answers]
        variation_correct_answers.extend(filler)
        filler = list(zip(variation_answers, variation_correct_answers))
        random.shuffle(filler)
        variation_answers, variation_correct_answers = zip(*filler)
        variation_correct_answers = list(filter(None, variation_correct_answers))
        return ClosedExercise(self._description, variation_answers, variation_correct_answers, self._images, self._time)
