from .exercise import ExerciseTemplate
from .closed_exercise import ClosedExerciseTemplate
from .boolean_exercise import BooleanExerciseTemplate
from .text_exercise import TextExerciseTemplate
