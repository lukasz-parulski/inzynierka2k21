import abc


class ExerciseTemplate:
    def __init__(self, description, answers_count, images, time):
        self._description = description
        self._answers_count = int(answers_count)
        self._images = images
        self._time = time

    @abc.abstractmethod
    def generate_variation(self):
        return

