import random

from .exercise import ExerciseTemplate
from ..boolean_exercise import BooleanExercise


class BooleanExerciseTemplate(ExerciseTemplate):
    def __init__(self, description, answers_count, statements, images, time):
        self._statements = statements
        super().__init__(description, answers_count, images, time)

    def generate_variation(self):
        variation_statements = random.sample(self._statements, self._answers_count)
        return BooleanExercise(self._description, variation_statements, self._images, self._time)
