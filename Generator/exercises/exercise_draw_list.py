import random


class ExerciseDrawList:
    def __init__(self, number_of_drawn_exercises):
        self._number_of_drawn_exercises = number_of_drawn_exercises
        self._exercise_list = []
        self._pop_exercise_list = []

    def get_random_exercise(self):
        assert len(self._exercise_list) > 0, "Exercise list is empty"

        if len(self._pop_exercise_list) <= 0:
            self._pop_exercise_list = self._exercise_list.copy()
        index = random.randrange(0, len(self._pop_exercise_list))
        exercise = self._pop_exercise_list.pop(index)
        return exercise

    def add_exercise_to_list(self, exercise):
        self._exercise_list.append(exercise)

    def get_number_of_drawn_exercises(self):
        return int(self._number_of_drawn_exercises)
