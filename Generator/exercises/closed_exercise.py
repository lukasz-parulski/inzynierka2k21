from .exercise import Exercise


class ClosedExercise(Exercise):
    def __init__(self, description, answers, correct_answers, images, time):
        self._answers = answers
        self._correct_answers = correct_answers
        super().__init__(description, images, time)

    def get_answers(self):
        return self._answers

    def get_correct_answers(self):
        return self._correct_answers
