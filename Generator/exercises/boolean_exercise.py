from .exercise import Exercise


class BooleanExercise(Exercise):
    def __init__(self, description, statements, images, time):
        self._statements = statements
        super().__init__(description, images, time)

    def get_statements(self):
        return self._statements
