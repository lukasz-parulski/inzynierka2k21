class Exercise:
    def __init__(self, description, images, time):
        self._description = description
        self._images = images
        self._time = time

    def get_description(self):
        return self._description

    def get_images(self):
        return self._images

    def get_time(self):
        return self._time