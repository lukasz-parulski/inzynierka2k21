from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
import os


class GoogleDriveHandler:
    @staticmethod
    def upload_to_drive(folder_to_upload: str, google_id='1hISwkHtJATDWUda8gexqo2UpD8c0YUPr'):
        """
        :param folder_to_upload: path of folder to upload to Google Drive
        :param google_id: id of destination Google Drive folder
        Uploads files from local folder to Google Drive folder.
        """
        name_list = os.listdir(folder_to_upload)
        upload_file_list = [folder_to_upload+'/' + x for x in name_list]
        gauth = GoogleAuth()
        drive = GoogleDrive(gauth)
        for upload_file, name in zip(upload_file_list, name_list):
            gfile = drive.CreateFile({'parents': [{'id': google_id}]})
            gfile.SetContentFile(upload_file)
            gfile['title'] = name
            gfile.Upload()
