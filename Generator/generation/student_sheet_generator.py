from ..generation.student_answer_generator import StudentAnswerGenerator
from ..exercises.templates import TextExerciseTemplate, ClosedExerciseTemplate, BooleanExerciseTemplate


class StudentSheetGenerator:
    def __init__(self, closed_exercise_templates, boolean_exercise_templates, text_exercise_templates,
                 lists_of_drawn_exercises):
        self._closed_exercise_variations = []
        self._boolean_exercise_variations = []
        self._text_exercise_variations = []
        self._closed_exercise_templates = closed_exercise_templates
        self._boolean_exercise_templates = boolean_exercise_templates
        self._text_exercise_templates = text_exercise_templates
        self._lists_of_drawn_exercises = lists_of_drawn_exercises
        self._student_answer_sheet = StudentAnswerGenerator(self._closed_exercise_variations,
                                                            self._boolean_exercise_variations,
                                                            self._text_exercise_variations)

    def _generate_closed_exercise_variations(self, closed_exercise_templates):
        self._closed_exercise_variations.clear()
        for template in closed_exercise_templates:
            self._closed_exercise_variations.append(template.generate_variation())

    def _generate_boolean_exercise_variations(self, boolean_exercise_templates):
        self._boolean_exercise_variations.clear()
        for template in boolean_exercise_templates:
            self._boolean_exercise_variations.append(template.generate_variation())

    def _generate_text_exercise_variations(self, text_exercise_templates):
        self._text_exercise_variations.clear()
        for template in text_exercise_templates:
            self._text_exercise_variations.append(template.generate_variation())

    def generate_student_sheet(self, student_id):
        self._generate_closed_exercise_variations(self._closed_exercise_templates)
        self._generate_boolean_exercise_variations(self._boolean_exercise_templates)
        self._generate_text_exercise_variations(self._text_exercise_templates)

        for drawn in self._lists_of_drawn_exercises:
            for i in range(0, drawn.get_number_of_drawn_exercises()):
                drawn_exercise = drawn.get_random_exercise()
                if isinstance(drawn_exercise, ClosedExerciseTemplate):
                    self._closed_exercise_variations.append(drawn_exercise.generate_variation())
                elif isinstance(drawn_exercise, BooleanExerciseTemplate):
                    self._boolean_exercise_variations.append(drawn_exercise.generate_variation())
                elif isinstance(drawn_exercise, TextExerciseTemplate):
                    self._text_exercise_variations.append(drawn_exercise.generate_variation())

        self._student_answer_sheet.generate_answer_key(student_id)

        closed_questions = [
            {
                "question": exercise.get_description(),
                "images": exercise.get_images(),
                "time": exercise.get_time(),
                "answers": exercise.get_answers()
            }
            for exercise in self._closed_exercise_variations
        ]

        boolean_questions = [
            {
                "question": exercise.get_description(),
                "images": exercise.get_images(),
                "time": exercise.get_time(),
                "statements": [statement.text for statement in exercise.get_statements()]
            }
            for exercise in self._boolean_exercise_variations
        ]

        text_questions = [
            {
                "question": exercise.get_description(),
                "images": exercise.get_images(),
                "time": exercise.get_time(),
                "text": exercise.get_texts()
            }
            for exercise in self._text_exercise_variations
        ]

        return {
            "student_id": student_id,
            "closed_questions": closed_questions,
            "boolean_questions": boolean_questions,
            "text_questions": text_questions
        }
