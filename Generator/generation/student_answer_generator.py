import csv


class StudentAnswerGenerator:
    def __init__(self, closed_exercises, boolean_exercises, text_exercises):
        self._closed_exercises = closed_exercises
        self._closed_exercises_answer_keys = []
        self._boolean_exercises = boolean_exercises
        self._boolean_exercises_answer_keys = []
        self._text_exercises = text_exercises
        self._text_exercises_answer_keys = []

    def _generate_closed_exercises_answer_key(self, closed_exercises):
        self._closed_exercises_answer_keys.clear()

        for ex in closed_exercises:
            answers = []
            closed_exercises_string_line = ""

            for ans in ex.get_correct_answers():
                if closed_exercises_string_line != "":
                    closed_exercises_string_line += ", "
                closed_exercises_string_line += ans
            answers.append(closed_exercises_string_line)
            self._closed_exercises_answer_keys.append(answers)

    def _generate_boolean_exercises_answer_key(self, boolean_exercises):
        self._boolean_exercises_answer_keys.clear()
        for ex in boolean_exercises:
            answers = []

            for ans in ex.get_statements():
                answers.append(ans.answer)
            self._boolean_exercises_answer_keys.append(answers)

    def _generate_text_exercises_answer_key(self, text_exercises):
        self._text_exercises_answer_keys.clear()

        for ex in text_exercises:
            answers = []
            for ans in ex.get_answers():
                answers.append(ans)
            self._text_exercises_answer_keys.append(answers)

    def generate_answer_key(self, student_id):
        self._generate_closed_exercises_answer_key(self._closed_exercises)
        self._generate_boolean_exercises_answer_key(self._boolean_exercises)
        self._generate_text_exercises_answer_key(self._text_exercises)

        filename = "../data/answer_keys/"+student_id + "_answer_key.csv"
        with open(filename, 'w', encoding='UTF8', newline='') as f:
            writer = csv.writer(f, delimiter=";")
            for ex in self._boolean_exercises_answer_keys:
                writer.writerow(ex)
            for ex in self._closed_exercises_answer_keys:
                writer.writerow(ex)
            for ex in self._text_exercises_answer_keys:
                writer.writerow(ex)
