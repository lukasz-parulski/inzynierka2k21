from ..generation.student_sheet_generator import StudentSheetGenerator
from ..xmlFolder import XMLTemplateLoader
import csv


class OutputDictGenerator:
    def __init__(self, student_ids_path, xml_template_path, xml_modules_path=None):
        self._loader = XMLTemplateLoader(xml_template_path, xml_modules_path)
        self._student_sheet_generator = StudentSheetGenerator(
            self._loader.get_closed_exercise_templates(),
            self._loader.get_boolean_exercise_templates(),
            self._loader.get_text_exercise_templates(),
            self._loader.get_lists_of_exercises())

        self._student_ids = self._load_student_ids(student_ids_path)

    @staticmethod
    def _load_student_ids(student_ids_path):
        ids = []
        with open(student_ids_path) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')
            for row in csv_reader:
                ids.append(''.join(row))
        ids.pop(0)
        return ids

    def generate_output_dict(self):
        output_dict = []
        for student_id in self._student_ids:
            output_dict.append(self._student_sheet_generator.generate_student_sheet(student_id))
        return output_dict
