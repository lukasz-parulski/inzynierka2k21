from Generator.generation import OutputDictGenerator

import xml.etree.cElementTree as ET
import xml.dom.minidom as md


class SheetGenerator:
    """
    Class for creating xml form of an exam for each student and saving it on Google Drive.
    """
    def __init__(self, output_directory: str, indexes: str, exam_template: str, xml_modules_path: str = None):
        """
        :param output_directory: name of folder in which xml will be saved locally
        :param indexes: path of .txt file containing students' indexes
        :param exam_template: path of xml file containing exam template
        """
        self.__sheet_destination_dir = output_directory
        self.__generator = OutputDictGenerator(
            indexes,
            exam_template,
            xml_modules_path
        )

    def create_sheet_xml_google_form(self) -> None:
        """
        Creates xml files for each students personal sheet & saves them in __sheet_destination_dir folder.
        """
        sheets = self.__generator.generate_output_dict()

        for sheet in sheets:
            root = ET.Element("response")
            id = 1
            for question in sheet["boolean_questions"]:
                task = ET.SubElement(root, "task", id=str(id))
                type = ET.SubElement(task, "type", name="Type")
                ET.SubElement(type, "value").text = "Tak-Nie"
                column = ET.SubElement(type, "column", name="Title")
                ET.SubElement(column, "value").text = question["question"]

                for image in question["images"]:
                    column = ET.SubElement(type, "column", name="ImageName")
                    ET.SubElement(column, "value").text = image

                for statement in question["statements"]:
                    column = ET.SubElement(type, "column", name="Questions")
                    ET.SubElement(column, "value").text = statement
                id += 1

            for question in sheet["closed_questions"]:
                task = ET.SubElement(root, "task", id=str(id))
                type = ET.SubElement(task, "type", name="Type")
                ET.SubElement(type, "value").text = "Wielokrotnego"
                column = ET.SubElement(type, "column", name="Question")
                ET.SubElement(column, "value").text = question["question"]

                for image in question["images"]:
                    column = ET.SubElement(type, "column", name="ImageName")
                    ET.SubElement(column, "value").text = image

                for answer in question["answers"]:
                    column = ET.SubElement(type, "column", name="Answers")
                    ET.SubElement(column, "value").text = answer
                id += 1

            for question in sheet["text_questions"]:
                task = ET.SubElement(root, "task", id=str(id))
                type = ET.SubElement(task, "type", name="Type")
                ET.SubElement(type, "value").text = "Otwarte"
                column = ET.SubElement(type, "column", name="Title")
                ET.SubElement(column, "value").text = question["question"]

                for image in question["images"]:
                    column = ET.SubElement(type, "column", name="ImageName")
                    ET.SubElement(column, "value").text = image

                for text in question["text"]:
                    column = ET.SubElement(type, "column", name="Questions")
                    ET.SubElement(column, "value").text = text
                id += 1

            tree = ET.ElementTree(root)
            filename = self.__sheet_destination_dir + "/" + sheet["student_id"] + ".xml"
            tree.write(filename, encoding="utf-8", xml_declaration=True)

            file = md.parse(filename)

            cdata_list = file.getElementsByTagName('value')
            for cdata in cdata_list:
                child = cdata.firstChild
                cdata_section = file.createCDATASection(child.data)
                cdata.appendChild(cdata_section)
                cdata.removeChild(child)

            with open(filename, "w", encoding='utf-8') as fs:
                fs.write(file.toprettyxml())
                fs.close()

            self.create_sheet_xml_online_service(sheets)

    def create_sheet_xml_online_service(self, sheets) -> None:
        """
        Creates xml files for each students personal sheet & saves them in __sheet_destination_dir folder.
        """

        for sheet in sheets:
            root = ET.Element("exam", id="exam id TODO")
            id = 1
            for question in sheet["boolean_questions"]:
                task = ET.SubElement(root, "task", id=str(id), type="Yes-No", time=question["time"])
                title = ET.SubElement(task, "title")
                title.text = question["question"]

                for image in question["images"]:
                    image_element = ET.SubElement(task, "image")
                    image_element.text = image

                questions = ET.SubElement(task, "questions")
                for statement in question["statements"]:
                    value = ET.SubElement(questions, "value")
                    value.text = statement
                id += 1

            for question in sheet["closed_questions"]:
                task = ET.SubElement(root, "task", id=str(id), type="Closed", time=question["time"])
                title = ET.SubElement(task, "title")
                title.text = question["question"]

                for image in question["images"]:
                    image_element = ET.SubElement(task, "image")
                    image_element.text = image

                questions = ET.SubElement(task, "questions")
                for answer in question["answers"]:
                    value = ET.SubElement(questions, "value")
                    value.text = answer

                id += 1

            for question in sheet["text_questions"]:
                task = ET.SubElement(root, "task", id=str(id), type="Opened", time=question["time"])
                title = ET.SubElement(task, "title")
                title.text = question["question"]

                for image in question["images"]:
                    image_element = ET.SubElement(task, "image")
                    image_element.text = image

                questions = ET.SubElement(task, "questions")
                for text in question["text"]:
                    value = ET.SubElement(questions, "value")
                    value.text = text

                id += 1

            tree = ET.ElementTree(root)
            filename = "../data/xml_sheets_service" + "/" + sheet["student_id"] + ".xml"
            tree.write(filename, encoding="utf-8", xml_declaration=True)

            file = md.parse(filename)

            cdata_list = []

            cdata_list.extend(file.getElementsByTagName('title'))
            cdata_list.extend(file.getElementsByTagName('image'))
            cdata_list.extend(file.getElementsByTagName('value'))

            for cdata in cdata_list:
                child = cdata.firstChild
                cdata_section = file.createCDATASection(child.data)
                cdata.appendChild(cdata_section)
                cdata.removeChild(child)

            with open(filename, "w", encoding='utf-8') as fs:
                fs.write(file.toprettyxml())
                fs.close()
