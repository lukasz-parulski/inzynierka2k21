from Generator.generation.sheet_generator import SheetGenerator
from Generator.generation import GoogleDriveHandler


class CommandLineInterface:

    def __init__(self, mode: int):
        self.mode = mode
        self.run()

    def run(self):
        if self.mode == 1:
            self.main_view_students_number()
        elif self.mode == 2:
            self.main_view_students_indexes()
        else:
            raise(ValueError("Available modes: 1, 2"))

    @staticmethod
    def main_view_students_number():
        pass    # TODO students number input

    @staticmethod
    def main_view_students_indexes():
        XML_FOLDER_ID = "1hISwkHtJATDWUda8gexqo2UpD8c0YUPr"
        IMAGES_FOLDER_ID = "1WAHX7Pu98a-snz8BX_dd-joEzG8uoZrR"
        INDEXES_FOLDER_ID = "1RbBpUHrrQ9Jw6EHA3bCOMkQaKg4pOdUc"

        indexes_path = input("Students indexes txt file path:\n>> ")
        if indexes_path == "":
            indexes_path = "../data/indexes/indexes.csv"
        exam_template = input("Exam template xml path:\n>> ")
        if exam_template == "":
            exam_template = "../developer_data/proofOfConcept.xml"
        modules_data = input("Modules template xml path (../developer_data/xml_modules/):\n>> ")
        if modules_data == "":
            modules_data = None
        output_directory = input("Output directory:\n>> ")
        if output_directory == "":
            output_directory = "../data/xml_sheets"
        generator = SheetGenerator(output_directory, indexes_path, exam_template, modules_data)
        generator.create_sheet_xml_google_form()

        if input("Upload to google drive? (y/n)\n>> ") != 'y':
            return
        google_id = input("Google drive folder id:\n>> ")
        if google_id == "":
            GoogleDriveHandler.upload_to_drive(output_directory)
            GoogleDriveHandler.upload_to_drive("../data/images", IMAGES_FOLDER_ID)
            GoogleDriveHandler.upload_to_drive("../data/indexes", INDEXES_FOLDER_ID)
        else:
            GoogleDriveHandler.upload_to_drive(output_directory, XML_FOLDER_ID)
            GoogleDriveHandler.upload_to_drive("../data/images", IMAGES_FOLDER_ID)
            GoogleDriveHandler.upload_to_drive("../data/indexes", INDEXES_FOLDER_ID)
