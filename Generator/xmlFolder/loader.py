from xml.etree.ElementTree import parse
from xml.etree import ElementTree
from Generator.exercises import BooleanStatement, ExerciseDrawList
from Generator.exercises.templates import ClosedExerciseTemplate, BooleanExerciseTemplate, TextExerciseTemplate
import os


class XMLTemplateLoader:
    CLOSED_EXERCISE_TAG = "closedExercise"
    BOOLEAN_EXERCISE_TAG = "booleanExercise"
    TEXT_EXERCISE_TAG = "textExercise"
    DESCRIPTION_TAG = "description"
    TIME_TAG = "time"
    IMAGE_TAG = "image"
    CORRECT_ANSWER_TAG = "correctAnswer"
    INCORRECT_ANSWER_TAG = "incorrectAnswer"
    BOOLEAN_EXERCISE_ANSWER_TAG = "answer"
    TEXT_EXERCISE_ANSWER_TAG = "answer"
    TEXT_EXERCISE_TEXT_TAG = "textQuestion"
    ANSWERS_COUNT_TAG = "numberOfAnswers"
    CORRECT_ANSWERS_COUNT_TAG = "numberOfCorrectAnswers"
    EXERCISE_LIST_TAG = "drawListExercise"
    NUMBER_OF_EXERCISES_TO_DRAW_TAG = "numberOfExercisesToChoose"

    def __init__(self, path, module_folder_path=None):
        self.__file_path = path
        self.__closed_exercise_templates = []
        self.__boolean_exercise_templates = []
        self.__text_exercise_templates = []
        self.__lists_of_exercises = []
        self.__paramount_time_per_exercise = "None"
        self.__xml_tree_root = parse(self.__file_path).getroot()
        self.__load_exercises()
        self.__module_folder_path = module_folder_path
        self.__validate_module_folder_path()

    def __validate_module_folder_path(self):
        if self.__module_folder_path is not None:
            self.__load_additional_xml_files()

    def __load_exercises(self):
        for elem in self.__xml_tree_root:
            if elem.tag == self.CLOSED_EXERCISE_TAG:
                self.__closed_exercise_templates.append(self.__load_closed_exercise(elem))
            elif elem.tag == self.BOOLEAN_EXERCISE_TAG:
                self.__boolean_exercise_templates.append(self.__load_boolean_exercise(elem))
            elif elem.tag == self.TEXT_EXERCISE_TAG:
                self.__text_exercise_templates.append(self.__load_text_exercise(elem))
            elif elem.tag == self.EXERCISE_LIST_TAG:
                number_of_exercise_to_draw = elem.get(self.NUMBER_OF_EXERCISES_TO_DRAW_TAG)
                list_time = elem.get(self.TIME_TAG)
                new_list = ExerciseDrawList(number_of_exercise_to_draw)

                for exercise in elem:
                    if exercise.tag == self.CLOSED_EXERCISE_TAG:
                        new_list.add_exercise_to_list(self.__load_closed_exercise(exercise, list_time))
                    elif exercise.tag == self.BOOLEAN_EXERCISE_TAG:
                        new_list.add_exercise_to_list(self.__load_boolean_exercise(exercise, list_time))
                    elif exercise.tag == self.TEXT_EXERCISE_TAG:
                        new_list.add_exercise_to_list(self.__load_text_exercise(exercise, list_time))
                self.__lists_of_exercises.append(new_list)

    def __load_additional_xml_files(self):
        """
        Method which finds all xml files in specific folder and loads exercises from them into the sheet
        """
        for file in os.listdir(self.__module_folder_path):
            if file.endswith('.xml'):
                self.__xml_tree_root = parse(self.__module_folder_path + file).getroot()
                self.__load_exercises()

    def __set_time_of_exercise(self, time, paramount_time):
        if time is None:
            if paramount_time is None:
                time = self.__paramount_time_per_exercise
            else:
                time = paramount_time
        return time

    @staticmethod
    def __get_text_of_tag(element):
        if element.attrib:
            element.attrib.clear()

        string = element.text
        if string is None:
            string = " "
        for child in element:
            string += ElementTree.tostring(child, encoding='unicode')
        return string

    def __load_closed_exercise(self, exercise_xml_element, paramount_time=None):
        description = self.__get_text_of_tag(exercise_xml_element.find(self.DESCRIPTION_TAG))
        images = [img.text for img in exercise_xml_element.findall(self.IMAGE_TAG)]
        answers_count = exercise_xml_element.get(self.ANSWERS_COUNT_TAG)
        time = exercise_xml_element.get(self.TIME_TAG)
        time = self.__set_time_of_exercise(time, paramount_time)

        correct_answers_count = exercise_xml_element.get(self.CORRECT_ANSWERS_COUNT_TAG)
        correct_answers = [self.__get_text_of_tag(ans) for ans in exercise_xml_element.findall(self.CORRECT_ANSWER_TAG)]
        incorrect_answers = [self.__get_text_of_tag(ans) for ans in exercise_xml_element.findall(self.INCORRECT_ANSWER_TAG)]

        return ClosedExerciseTemplate(
            description, answers_count, correct_answers_count, correct_answers, incorrect_answers, images, time)

    def __load_boolean_exercise(self, exercise_xml_element, paramount_time=None):
        description = self.__get_text_of_tag(exercise_xml_element.find(self.DESCRIPTION_TAG))
        images = [img.text for img in exercise_xml_element.findall(self.IMAGE_TAG)]
        answers_count = exercise_xml_element.get(self.ANSWERS_COUNT_TAG)
        statements = [BooleanStatement(statement, statement.get("result"))
                      for statement in exercise_xml_element.findall(self.BOOLEAN_EXERCISE_ANSWER_TAG)]
        time = exercise_xml_element.get(self.TIME_TAG)
        time = self.__set_time_of_exercise(time, paramount_time)

        for statement in statements:
            statement.text = self.__get_text_of_tag(statement.text)

        return BooleanExerciseTemplate(description, answers_count, statements, images, time)

    def __load_text_exercise(self, exercise_xml_element, paramount_time=None):
        description = self.__get_text_of_tag(exercise_xml_element.find(self.DESCRIPTION_TAG))
        images = [img.text for img in exercise_xml_element.findall(self.IMAGE_TAG)]
        answers_count = exercise_xml_element.get(self.ANSWERS_COUNT_TAG)
        time = exercise_xml_element.get(self.TIME_TAG)
        time = self.__set_time_of_exercise(time, paramount_time)

        questions = [ans for ans in exercise_xml_element.findall(self.TEXT_EXERCISE_TEXT_TAG)]
        answers = [ans.get("answer") for ans in questions]
        questions = [self.__get_text_of_tag(ans) for ans in questions]

        return TextExerciseTemplate(description, answers_count, questions, answers, images, time)

    def get_closed_exercise_templates(self):
        return self.__closed_exercise_templates

    def get_boolean_exercise_templates(self):
        return self.__boolean_exercise_templates

    def get_text_exercise_templates(self):
        return self.__text_exercise_templates

    def get_lists_of_exercises(self):
        return self.__lists_of_exercises
